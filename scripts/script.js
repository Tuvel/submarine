// Game Properties:
const CLICK_RESULT_TIME = 1 // seconds
const MS_IN_SECOND = 1024
const DEFAULT_TILE_TEXT = '?'
const SAME_ROW_INDEX_TEXT = 'Close'
const ONE_BLOCK_AWAY_TEXT = 'Almost'
const CORRECT_LOCATION = 'Correct'
const NOT_CLOSE_TEXT = 'X'

// gets a min value and a max value, returns a random Integer between those two numbers.
function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}

// gets two integers for the submarine location index and returns a random location on the board.
function getRandomSubmarineLocation(boardSize) {
    return [getRndInteger(0, boardSize - 1), getRndInteger(0, boardSize - 1)]
}


// Setting the board size:
let BOARD_SIZE = prompt('Insert a board size for the game')
let clicks = 0

// Building the board using the board size:
let gameBoardDiv = document.querySelector('.game-board')
for (let row = 0; row < BOARD_SIZE; ++row) {
    let rowDiv = document.createElement('div')
    rowDiv.setAttribute('class', 'row')
    rowDiv.setAttribute('id', ('row' + (row + 1)))
    for (let tile = 0; tile < BOARD_SIZE; ++tile) {
        let tileDiv = document.createElement('div')
        tileDiv.setAttribute('class', 'tile')
        let tileInnerSpan = document.createElement('span')
        tileInnerSpan.innerText = DEFAULT_TILE_TEXT
        tileInnerSpan.setAttribute('class', 'unselectable')
        tileDiv.appendChild(tileInnerSpan)
        rowDiv.appendChild(tileDiv)
    }
    gameBoardDiv.appendChild(rowDiv)
}


// Inserting the submarine to a random tile:
let submarine = getRandomSubmarineLocation(BOARD_SIZE)


// gets a click location (row, index) and a submarine location (row, index)
// returns the result text for the click
function getClickResult(clickLocation, submarineLocation) {
    let rowDistance = Math.abs(clickLocation[0] - submarineLocation[0])
    let indexDistance = Math.abs(clickLocation[1] - submarineLocation[1])
    let distance = Math.abs(rowDistance + indexDistance)
    if (distance == 0) {
        return CORRECT_LOCATION
    }
    else if (distance === 1) {
        return ONE_BLOCK_AWAY_TEXT
    }
    else if (clickLocation[0] === submarineLocation[0] || clickLocation[1] === submarineLocation[1]) {
        return SAME_ROW_INDEX_TEXT
    }
    return NOT_CLOSE_TEXT
}


function updateTriesCounter() {
    ++clicks
    document.getElementById('clicks').innerText = clicks
}


function hideGameBoard() {
    gameBoardDiv['style']['display'] = 'none'
    setTimeout(function() {
        alert('You won the game with ' + clicks + ' tries!')
    }, 1)
    document.getElementById('new-game-btn')['style']['display'] = 'flex'
}


// When the user clicks on any tile:
function tileClick(row, index, tileElement) {
    updateTriesCounter()
    let tileSpan = tileElement.querySelector('span')
    let resultText = getClickResult([row, index], submarine)
    if (resultText === CORRECT_LOCATION) {
        hideGameBoard()
    }
    tileSpan.innerText = resultText
    setTimeout(function() {
        tileSpan.innerText = DEFAULT_TILE_TEXT
    }, CLICK_RESULT_TIME * MS_IN_SECOND);
}


// Set a click listener for every tile:
let tiles = document.querySelectorAll('.tile')
let tileIndex = 0
tiles.forEach(tile => {
    let row = parseInt(tileIndex / BOARD_SIZE % BOARD_SIZE)
    let indexInRow = (tileIndex % BOARD_SIZE)
    tile.addEventListener('click', function() {
        tileClick(row, indexInRow, tile)
    })
    ++tileIndex
})


function refresh() {
    window.location.reload()
}